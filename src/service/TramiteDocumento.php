<?php
require_once('/../dao/banco.php');
require_once('/../dao/tramiteDocumento.php');
class TramiteDocumentoService{
    private $lst1;
	private $lst2;
    public function __construct(){
		$bd = new TramiteDocumentoDAO();
		$this->lst1= $bd->lstEstadoTramite();
		$this->lst2= $bd->lstPersona();
    }

	public function lstEstadoTramite(){
        return json_encode($this->lst1);
    }
	public function lstPersona(){
        return json_encode($this->lst2);
    }
}
/*
class Banco{
	function lstBanco(){
		define('BD_SERVIDOR', 'localhost');
	define('BD_NOMBRE', 'examen');
	define('BD_USUARIO', 'root');
	define('BD_PASSWORD', '');
	$db = new PDO('mysql:host=' . BD_SERVIDOR . ';dbname=' . BD_NOMBRE . ';charset=utf8', BD_USUARIO, BD_PASSWORD);
		 $consulta = $db->prepare("select * from banco");
				$consulta->execute();
				// Almacenamos los resultados en un array asociativo.
				$resultados = $consulta->fetchAll(PDO::FETCH_ASSOC);
				// Devolvemos ese array asociativo como un string JSON.
				echo json_encode($resultados);
	}
}
*/
?>