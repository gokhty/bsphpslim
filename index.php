<?php
require_once 'vendor/autoload.php';
require_once 'src/action/banco.php';
require_once 'src/action/tramiteDocumento.php';

include('cabezera.php');//cabezera para la solución en cors

$app = new Slim\App();

$app->get('/hello/{name}', function ($request, $response, $args) {
    return $response->write("Hello, " . $args['name']);
});

$app->get('/banco', function() {
	$b = new BancoAction();
	$b->mustraNombre();
});
$app->get('/sbanco', function() {
	$b = new BancoAction();
	$b->restBanco();
});
$app->get('/gestado', function() {
	$b = new TramiteDocumentoAction();
	$b->glstEstadoTramite();
});
$app->get('/gpersona', function() {
	$b = new TramiteDocumentoAction();
	$b->glstPersona();
});
$app->get('/pagina/{name}', function ($request, $response, $args) {
    include("web/banco.php");
});
$app->post("/api", function($request, $response, $arguments) {
	$b = new BancoAction();
	//$b->insertaBancop();
	$b->insertaBanco();
});
$app->post("/insertaTramite", function($request, $response, $arguments) {
	$b = new TramiteDocumentoAction();
	$b->insertaTramite();
});
$app->post("/ejemplopost", function($request, $response, $args) {
	$reqPost = $request->getParsedBody();
	$val1 = $reqPost["val1"];
	$val2 = $reqPost["val2"];
	
	$response->write("valores: ". $val1 ." ". $val2);
	return $response;
});
$app->get('/tramite', function ($request, $response, $args) {
    include("web/a.php");
});
$app->run();
?>