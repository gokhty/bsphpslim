drop database examen;
create database examen;
use examen;
create table banco(
id int primary key auto_increment,
nom varchar(255)
);
insert into banco(nom)
values
('Interbank'),('Continental'),('Nación');

select * from banco;

-- -------------------------
create table estadoTramite(
id int primary key auto_increment,
des varchar(45)
);
create table persona(
id int primary key auto_increment,
nom varchar(45),
tipo int -- 1 enidad, 2 natural 3 jurídica
);
create table tramites(
id int primary key auto_increment,
nom varchar(45),
asunto varchar(255),
pers int,
fecIng date,
plazo int,
estado int,
fec date,
foreign key(estado) references estadoTramite(id),
foreign key(pers) references persona(id)
);
-- --------------------------------------------------
insert into estadoTramite(des)values
('RESPONDIDO'),('EN TRÁMITE'),('OBSERVADO');
insert into persona(nom,tipo) values
('Municipalidad de San Juan de Lurigancho',1),
('Supervisor',2),
('SEDAPAL',3);
delimiter |
create procedure sp_insertaTramite
(
pnom varchar(45),
pasunto varchar(255),
ppers varchar(255),
pfecIng date,
pplazo int,
pestado int,
pfec date
)
begin
declare i int;
set i = (select id from persona where nom = ppers);
insert into tramites(nom, asunto, pers, fecIng, plazo, estado, fec)values
(pnom, pasunto, i, pfecIng, pplazo, pestado, pfec);
end
|
select * from tramites;
call sp_insertaTramite('sdfa','asfd','Municipalidad de San Juan de Lurigancho','2019-08-31',30,1,'2019-09-01');