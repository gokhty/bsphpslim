<?php
//require_once('/../conexion.php');
require_once('conexion.php');
class Banco{
    private $db;
	private $res; //guarda el resultado al cambiar la contraseña
	private $loginn;
    public function __construct(){
		$bd = new conexion();
		$this->db= $bd->getConexion();
		$this->res=array();
		$this->loginn=array();
    }

	public function lstBanco(){
        $consulta=$this->db->prepare("select * from banco");
		$consulta->execute();
		$resultados = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $resultados; 
		$consulta = null;
		$this->db = null; 
    }
	
	public function insertaBanco($a){
        $consulta=$this->db->prepare("insert into banco(nom) values(?)");
		$consulta->bindParam(1,$a);
		$consulta->execute();
		$consulta = null;
		$this->db = null; 
    }
}
/*
class Banco{
	function lstBanco(){
		define('BD_SERVIDOR', 'localhost');
	define('BD_NOMBRE', 'examen');
	define('BD_USUARIO', 'root');
	define('BD_PASSWORD', '');
	$db = new PDO('mysql:host=' . BD_SERVIDOR . ';dbname=' . BD_NOMBRE . ';charset=utf8', BD_USUARIO, BD_PASSWORD);
		 $consulta = $db->prepare("select * from banco");
				$consulta->execute();
				// Almacenamos los resultados en un array asociativo.
				$resultados = $consulta->fetchAll(PDO::FETCH_ASSOC);
				// Devolvemos ese array asociativo como un string JSON.
				echo json_encode($resultados);
	}
}
*/
?>