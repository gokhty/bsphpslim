<?php
//require_once('/../conexion.php');
require_once('conexion.php');
class TramiteDocumentoDAO{
    private $db;
	private $res; //guarda el resultado al cambiar la contraseña
	private $loginn;
    public function __construct(){
		$bd = new conexion();
		$this->db= $bd->getConexion();
		$this->res=array();
		$this->loginn=array();
    }

	public function lstEstadoTramite(){
        $consulta=$this->db->prepare("select * from estadoTramite");
		$consulta->execute();
		$resultados = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $resultados; 
		$consulta = null;
		$this->db = null; 
    }
	public function lstPersona(){
        $consulta=$this->db->prepare("select * from persona");
		$consulta->execute();
		$resultados = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $resultados; 
		$consulta = null;
		$this->db = null; 
    }
	
	public function insertaTramite($a,$b,$c,$d,$e,$f,$g){
        $consulta=$this->db->prepare("call sp_insertaTramite(?,?,?,?,?,?,?)");
		$consulta->bindParam(1,$a);
		$consulta->bindParam(2,$b);
		$consulta->bindParam(3,$c);
		$consulta->bindParam(4,$d);
		$consulta->bindParam(5,$e);
		$consulta->bindParam(6,$f);
		$consulta->bindParam(7,$g);
		$consulta->execute();
		$consulta = null;
		$this->db = null; 
    }
}
/*
class Banco{
	function lstBanco(){
		define('BD_SERVIDOR', 'localhost');
	define('BD_NOMBRE', 'examen');
	define('BD_USUARIO', 'root');
	define('BD_PASSWORD', '');
	$db = new PDO('mysql:host=' . BD_SERVIDOR . ';dbname=' . BD_NOMBRE . ';charset=utf8', BD_USUARIO, BD_PASSWORD);
		 $consulta = $db->prepare("select * from banco");
				$consulta->execute();
				// Almacenamos los resultados en un array asociativo.
				$resultados = $consulta->fetchAll(PDO::FETCH_ASSOC);
				// Devolvemos ese array asociativo como un string JSON.
				echo json_encode($resultados);
	}
}
*/
?>