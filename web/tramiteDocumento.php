<!DOCTYPE html>
<html>
<head>
	<title>titulo</title>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
</head>
<body ng-app="myApp" ng-controller="myCtrl">

<form action="/insertaTramite" method="POST">
<input type="text" name="nom" placeholder="Nombre"/>
<input type="text" name="asunto" placeholder="Asunto"/>

<input  list="testList" type="text" name="dlistPers" id="txtdis" placeholder="Persona/Entidad" required/>
    <datalist id="testList">
        <option ng-repeat="d in persona" value="{{d.nom}}">
    </datalist>
	
<input type="text" name="fecIng" placeholder="Fecha de ingreso"/>
<input type="text" name="plazo" placeholder="Plazo"/>
<select name="cboEstado">
	<option  ng-repeat="x in datos" value="{{x.id}}">{{x.des}}
</select>
<input type="date" name="fec" placeholder="Fecha"/>
<input type="submit"/>
</form>
</body>
<script>
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope, $http) {
  $http.get("/gestado")
  .then(function(response) {
      $scope.datos = response.data;
  });
  $http.get("/gpersona")
  .then(function(response) {
      $scope.persona = response.data;
  });
});
</script>
</html>